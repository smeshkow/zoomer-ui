import thunkMiddleware from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import { applyMiddleware, createStore, compose } from 'redux';
import { makeRootReducer } from '../reducers/index';

const middleware = applyMiddleware(thunkMiddleware, promiseMiddleware());

const enhancers = [middleware];
if (__DEV__) {
    const devToolsExtension = window.devToolsExtension;
    if (devToolsExtension) {
      enhancers.push(devToolsExtension());
    }
}

const store = createStore(
    makeRootReducer(),
    compose(...enhancers)
);

export default store;