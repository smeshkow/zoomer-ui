import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addArticle } from '../reducers/articles';
import { getAPIURL, getUIURL } from '../config/index';

const mapActionCreators = {
  addArticle
};

class ConnectedArticleForm extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      description: '',
      publishedDate: '',
      image: '',
      imagePreviewUrl: '',
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const { article } = this.props;
    if (article) {
      this.setState({
        title: article.title,
        description: article.body,
        publishedDate: article.publishedDate,
        // imagePreviewUrl: article.imageURL,
      });
    }
  }

  handleChange(event) {
    const target = event.target;
    // const value = target.type === 'file' ? event.target.files[0] : target.value;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });

    // if (target.type === 'file') {
    //   const reader = new FileReader();
    //   reader.onloadend = () => {
    //     this.setState({
    //       image: value,
    //       imagePreviewUrl: reader.result
    //     });
    //   }
    //   reader.readAsDataURL(value);
    // }
  }

  render() {
    const { title, description, publishedDate, imagePreviewUrl } = this.state;
    const { article, user } = this.props;

    let userDisplayName;
    let userID;
    if (user) {
      userDisplayName = (<input type="hidden" name="createdBy" value={user.displayName} />);
      userID = (<input type="hidden" name="createdByID" value={user.id} />);
    }

    let ImagePreview
    if (imagePreviewUrl) {
      ImagePreview = (<img className="img-fluid rounded" style={{maxWidth: '50%'}} src={imagePreviewUrl} />);
    }

    let publishedDateVal = publishedDate;
    if (!publishedDateVal) {
      publishedDateVal = new Date().toISOString();
    }

    let articleID = '';
    if (article) {
      articleID = article.id;
    }

    return (
      <div className="container">
        <form method="post" encType="multipart/form-data" action={getAPIURL() + '/posts' + articleID}>
          <div className="form-group">
            <label htmlFor="title">Title</label>
            <input
              type="text"
              className="form-control"
              id="title"
              name="title"
              value={title}
              onChange={this.handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="text">Text</label>
            <textarea
              className="form-control ckeditor"
              id="description"
              name="description"
              rows="4"
              cols="50"
              onChange={this.handleChange}
              value={description}
            />
            {/* <span style={{ display: 'none' }}>{CKEDITOR.replaceClass = 'ckeditor'}</span> */}
          </div>
          {/* <div className="form-group">
            <label htmlFor="image">Cover Image</label>
            <input type="file" className="form-control-file" id="image" name="image" onChange={this.handleChange} />
          </div> */}
          <div className="form-group">{ImagePreview}</div>
          <div className="form-group">
            <input type="hidden" id="publishedDate" name="publishedDate" value={publishedDateVal} />
            <input type="hidden" id="redirect" name="redirect" value={getUIURL() + '/articles/'} />
            {userDisplayName}
            {userID}
            <button type="submit" className="btn btn-success btn-lg">SAVE</button>
          </div>
        </form>
      </div>
    );
  }
}

ConnectedArticleForm.propTypes = {
  article: PropTypes.object,
  user: PropTypes.object,
}

const ArticleForm = connect(null, mapActionCreators)(ConnectedArticleForm);
export default ArticleForm;