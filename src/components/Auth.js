import React from 'react';
import { login, logout } from '../util/index';

const Auth = ({ user }) => (
    <span>
        {user && user.imageURL
            ? <span>
                <img className="rounded-circle" width="24" src={user.imageURL} style={{marginRight: '10px'}} />
                <button className="btn btn-dark btn-outline-light btn-sm" onClick={logout}>Log out</button>
              </span>
            : <button className="btn btn-outline-light btn-sm" onClick={login}>Log in</button>}
    </span>
);

export default Auth;
