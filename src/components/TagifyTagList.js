import React from 'react';

const TagifyTagList = ({ tags }) => (
    <ul className="nav">
        {tags.map((el, i) => (
            <li className="nav-item" key={i}>
                <span className="nav-link text-secondary" href="/">#{el.value}</span>
            </li>
        ))}
    </ul>
);

export default TagifyTagList;