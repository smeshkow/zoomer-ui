import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getArticleByID } from '../reducers/articles';
import ArticleForm from './ArticleForm';

const mapStateToProps = state => ({
    article: state.articles.currentArticle
});

const mapActionCreators = {
    getArticleByID
};

class ConnectedArticleEditForm extends Component {
    componentDidMount() {
        const { getArticleByID, match: { params } } = this.props;
        getArticleByID(params.id);
    }

    render() {
        const { article, user } = this.props;

        if (!article) {
            return null;
        }

        return (
            <ArticleForm article={article} user={user} />
        );
    }
}

ConnectedArticleEditForm.propTypes = {
    user: PropTypes.object,
    article: PropTypes.object,
}

const ArticleEditForm = connect(mapStateToProps, mapActionCreators)(ConnectedArticleEditForm);
export default ArticleEditForm;