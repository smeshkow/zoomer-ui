import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { getAPIURL } from '../config/index';

import './TagifyAbout.css';

const apiURL = getAPIURL();

const tagsForAnchorsExample = `
<script>
  tagifyjs.tagsForAnchors({
      // paste your ID from the step 3 in here
      appId: 'ID_provided_on_the_step_3',
      // CSS class of the pages links (i.e. <a> tags) 
      // you'd like to generate tags for
      anchorsClassName: 'my-awesome-article-link',
      // CSS class of the HTML tags you'd like 
      // to display tags in (should be next to the <a> tags)
      targetsClassName: 'my-awesome-article-tags',
      // address of the page with relevant pages for a tag
      pagesUrl: 'https://my-awesome-web-site.com/blog/relevant',
      // number of tags to generate
      tagLimit: 3
  });
</script>
`;

const pageTags = `
<script>
  tagifyjs.tagsForPage({
    // paste your ID from the step 3 in here
    appId: 'ID_provided_on_the_step_3',
    // CSS id of the HTML tag you'd like 
    // to display relevant tags/keywords in
    targetId: 'my-awesome-relevant-tags',
    // address of the page with relevant pages for a tag
    pagesUrl: 'https://my-awesome-web-site.com/blog/relevant',
    // number of tags to generate
    tagLimit: 3
  });
</script>
`;

const releavntExample = `
<div>
  <div id="relevant-pages"></div>
  <script>
    tagifyjs.relevant({
      // paste your ID from the step 3 in here
      appId: 'ID_provided_on_the_step_3',
      // CSS id of the HTML tag you'd 
      // like to display relevant stuff in
      targetId: 'relevant-pages'
    });
  </script>
</div>
`;

const loginExample = `
<div>
  <div id="tagify-login"></div>
  <script>
    tagifyjs.showLogin({
        // paste your ID from the step 3 in here
        appId: 'ID_provided_on_the_step_3',
        // CSS id of the HTML tag you'd 
        // like to display "Log in" button in
        targetId: 'tagify-login'
    });
  </script>
</div>
`;

const jsScript = `<script type="text/javascript" src="https://www.zoomio.org/tagifyjs/tagify.js"></script>`;


export default class TagifyAbout extends Component {
    render() {
        return (
            <div className="container">
                <h1>Welcome to Tagify</h1>
                <div>
                    <p>
                        Tagify is tool that helps to organize and find relevant data by simplifying it with automatic  processing.<br /><br />
                        In other words Tagify can process content on a web-site and provide relevant keywords-tags - <a href={`${apiURL}/blog/post/tags_as_a_service-5712840111423488`}>Tags-as-a-Service</a>.<br /><br />
                        For example, a <i>wiki-site</i> or a <i>blog</i> with a lot of content-pages can benefit from Tagify,
                        Tagify will generate clickable tags for the targeted pages, tags improve navigation
                        and will help discovering relevant content for the visitors, by clicking on them visitors will see
                        relevant pages which were tagged with the same tags.<br /><br />
                        And all of it with almost zero effort.<br /><br />
                        Got any questions? Feel free to ask in <a href="mailto:smeshkov@zoomio.org">email</a>.<br /><br />
                        Also checkout <a href="https://github.com/zoomio/tagify" target="_blank">Tagify</a> on the GitHub.<br /><br />
                        <Link className="btn btn-primary" to="/tagify" role="button">Go to playground</Link><br /><br />
                    </p>
                    <h4>How-to setup</h4>
                    <p>
                        More detailed guide can be found <a href={`${apiURL}/blog/post/integrating_tagify_on_a_website-5756936406433792`}>here</a>.
                        <br /><br />
                        <ul>
                            <li>Put the JavaScript library in the <code>&lt;head&gt;</code> of your web-site: <code>{jsScript}</code></li>
                            <li>Select a page for tagifying it</li>
                            <li>Display tags into the target DOM element</li>
                        </ul>
                    </p>
                    <p>
                        <button
                            className="btn btn-success btn-sm"
                            type="button"
                            data-toggle="collapse"
                            data-target="#collapseHowTo"
                            aria-expanded="false"
                            aria-controls="collapseHowTo">
                            Install "Tagify" on you web-site
                        </button>
                        <div className="how-to" style={{ marginTop: '10px' }}>
                            <div className="collapse" id="collapseHowTo">
                                <div className="card card-body">
                                    <dl className="row">
                                        <dt className="col-sm-3">1. Sign in</dt>
                                        <dd className="col-sm-9">Click "Log in" button at the top-right of the current page.</dd>

                                        <dt className="col-sm-3">2. Register a Web-site</dt>
                                        <dd className="col-sm-9">After you've logged in, you should be able to see "Register site" inside "Tagify" dropdown section in the top menu above, click on "Register site".</dd>

                                        <dt className="col-sm-3">3. Get Web-site ID</dt>
                                        <dd className="col-sm-9">On the "Register site" page put the address of the web-site into the "Site" input and click "Register" button, it will result in ID showed above. Copy the ID and save it, you'll need it later.</dd>

                                        <dt className="col-sm-3">4. Add TagifyJS to the web-site</dt>
                                        <dd className="col-sm-9">Insert <code>&lt;script type="text/javascript" src="https://www.zoomio.org/tagifyjs/tagify.js"&gt;&lt;/script&gt;</code> into the <code>&lt;head&gt;</code> section on the registered web-site.</dd>

                                        <dt className="col-sm-3">5. a) Mark pages in the list</dt>
                                        <dd className="col-sm-9">
                                            Use API provided by the TagifyJS library to generate tags for selected pages and place followng script under (after) the HTML tag which will contain generated keywords/tags:
                                            <pre><code>{tagsForAnchorsExample}</code></pre>
                                        </dd>

                                        <dt className="col-sm-3">5. b) Mark individual pages</dt>
                                        <dd className="col-sm-9">
                                            Place the following script under (after) the target container (HTML tag) for the generated tags, at the page where you display full body of the article/post:<br />
                                            <pre><code>{pageTags}</code></pre>
                                        </dd>

                                        <dt className="col-sm-3">6. Provide list of relevant</dt>
                                        <dd className="col-sm-9">
                                            Final step is to provide a page where you'll display relevant content for given tag/keyword.<br />
                                            It should be a separate page on which you'd need to add following just under (after) the HTML element that you'd want to contain the list of the relevant pages:<br />
                                            <pre><code>{releavntExample}</code></pre>
                                            Congratulations you are now all set and ready to use <b>Tagify</b>!
                                        </dd>

                                        <dt class="col-sm-3">7. Bonus step - Enable inline editting of tags for admin (you).</dt>
                                        <dd class="col-sm-9">
                                            To be able to edit the generated tags inline you'd need to authenticate first.<br />
                                            This can be done via the Tagify's "Log in" button.<br />
                                            Place following script under (after) the target container (HTML tag) in which you'd like to display the Tagify's "Log in" button:<br />
                                            <pre><code>{loginExample}</code></pre>
                                            After the button is placed and rendered you'll be able to authenticate yourself by clicking on it,<br />
                                            this will result in editing icons appearing next to the generated keywords/tags, so you can delete/add new tags.
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </p>
                    <p>
                        <br />
                        <img
                            src="https://storage.googleapis.com/www.zoomio.org/img/Tagify_a_website.PNG"
                            className="img-responsive about-img" />
                    </p>
                    <div>&nbsp;</div>
                </div>
                <div className="after-how-to">&nbsp;</div>
            </div>
        );
    }
}