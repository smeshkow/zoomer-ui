import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getImportStatus } from '../reducers/shimper';

const mapStateToProps = state => {
    const updates = [];
    const failed = [];

    const status = state.shimper.importStatus;

    if (status.input && status.input.records) {
        updates.push('parsed ' + status.input.records + ' records');
    }

    if (status.result && status.result.updates) {
        updates.push('prepared ' + status.result.updates + ' updates');
    }

    if (status.result && status.result.failed) {
        updates.push('failed ' + status.result.failed.length + ' updates');
        status.result.failed.forEach(element => {
            failed.push('[' + element.id + '] ' + element.title);
        });
    }

    return {
        isInProgres: status.isInProgres,
        file: status.input.fileName,
        updates,
        error: status.error,
        failed
    };
};

const mapActionCreators = {
    getImportStatus
};

class ConnectedShimperStatus extends Component {
    constructor(props) {
        super(props)
        this.checkStatus = this.checkStatus.bind(this);
    }

    componentDidMount() {
        this.interval = setInterval(this.checkStatus, 5000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    checkStatus() {
        this.props.getImportStatus();
    }

    render() {
        const { isInProgres = false, file, updates = [], error, failed = [] } = this.props;

        return (
            <div>
                {isInProgres && file ?
                    (
                        <div>
                            <div className="alert alert-primary" role="alert">
                                [in progress] <span>{file}</span>
                            </div>
                            {updates.length > 0 ? (
                                <div className="alert alert-primary" role="alert">
                                    <ul>
                                        {updates.map(el => (
                                            <li>{el}</li>
                                        ))}
                                    </ul>
                                </div>
                            ) : null}
                            {failed.length > 0 ? (
                                <div className="alert alert-primary" role="alert">
                                    <h3>Failed:</h3>
                                    <ul>
                                        {failed.map(el => (
                                            <li>{el}</li>
                                        ))}
                                    </ul>
                                </div>
                            ) : null}
                        </div>
                    ) : error && file ? (
                        <div className="alert alert-danger" role="alert">
                            [failed] <span>{file}</span> - <span>{error}</span>
                        </div>
                    ) : file ? (
                        <div className="alert alert-success" role="alert">
                            [success] <span>{file}</span>
                        </div>
                    ) : null}
            </div>
        );
    }
}

ConnectedShimperStatus.propTypes = {
    isInProgres: PropTypes.bool,
    file: PropTypes.string,
    updates: PropTypes.array,
    error: PropTypes.string,
    failed: PropTypes.array
}

const ShimperStatus = connect(mapStateToProps, mapActionCreators)(ConnectedShimperStatus);
export default ShimperStatus;