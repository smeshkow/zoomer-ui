import React from "react";
import PropTypes from "prop-types";
import { Provider } from "react-redux";
import App from "./App";

const Root = ({ store }) => (
    <div>
        <Provider store={store}>
            <App />
        </Provider>
    </div>
);

Root.propTypes = {
    store: PropTypes.object.isRequired
};

export default Root;