import React from 'react';
import Auth from './Auth';
import Menu from './Menu';
import { getUIURL } from '../config';

const AppHeader = ({ user }) => (
    <div className="container-fluid bg-dark">
        <div className="container" style={{ maxWidth: '900px' }}>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark" style={{ marginBottom: '20px' }}>
                <a className="navbar-brand" href={getUIURL()}>ZoomIO</a>
                <Menu user={user} />
                <div className="navbar-text navbar-right">
                    <Auth user={user} />
                </div>
            </nav>
        </div>
    </div>
);

export default AppHeader;