import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { registerSite } from '../reducers/tagify';
import { login, schema } from '../util/index';

import './TagifySiteRegister.css';

const mapStateToProps = state => ({
    registeredResult: state.tagify.registered,
    isLoading: state.tagify.isLoading
});

const mapActionCreators = {
    registerSite
};

class ConnectedTagifyRegister extends Component {
    constructor(props) {
        super(props);
        this.state = {
            site: '',
            loading: props.isLoading
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidUpdate() {
        const { isLoading } = this.props;
        const { loading } = this.state;

        if (isLoading !== loading) {
            this.setState({
                loading: isLoading
            });
        }
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const { isAuthenticated } = this.props;
        const { site } = this.state;

        if (!isAuthenticated) {
            login();
        }

        const protocol = schema();
        let siteValue = site;
        if (siteValue.indexOf(protocol) == -1) {
            siteValue = `${protocol}//${siteValue}`
        }

        this.props.registerSite(siteValue);
    }

    render() {
        const { isAuthenticated, registeredResult } = this.props;
        const { site, loading } = this.state;

        if (!isAuthenticated) {
            return null;
        }

        return (
            <div className="container">
                <h2 className="Page-title">Register site</h2>
                {loading ? (
                    <div className="spinner-border m-5 register-spinner" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                ) : registeredResult && registeredResult.id ?
                        (
                            <div className="alert alert-success" role="alert">
                                Your ID is <span>{registeredResult.id}</span>
                            </div>
                        ) : null}
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="site">Site</label>
                        <input
                            type="text"
                            className="form-control"
                            id="site"
                            name="site"
                            value={site}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-success btn-sm">Register</button>
                    </div>
                </form>
            </div>
        );
    }
}

ConnectedTagifyRegister.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    registeredResult: PropTypes.object,
}

const TagifyRegister = connect(mapStateToProps, mapActionCreators)(ConnectedTagifyRegister);
export default TagifyRegister;