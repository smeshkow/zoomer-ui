import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getArticleByID, deleteArticle } from '../reducers/articles';
import { redirectAPI } from '../util/index';
import { format } from 'date-fns';

import showdown from 'showdown';

import './ArticleDetail.css';

const MARKDOWN_BODY_TYPE = 3;

const classMap = {
    blockquote: 'mdBlockquote',
}

const bindings = Object.keys(classMap)
    .map(key => ({
        type: 'output',
        regex: new RegExp(`<${key}(.*)>`, 'g'),
        replace: `<${key} class="${classMap[key]}" $1>`
    }));

const MARKDOWN_CONVERTER = new showdown.Converter({
    openLinksInNewWindow: true,
    extensions: [...bindings]
});

const mapStateToProps = state => ({
    currentArticle: state.articles.currentArticle,
});

const mapActionCreators = {
    getArticleByID,
    deleteArticle
};


class ConnectedArticleDetail extends Component {
    constructor(props) {
        super(props)
        this.onEditClick = this.onEditClick.bind(this);
        this.onDeleteClick = this.onDeleteClick.bind(this);
    }

    componentDidMount() {
        const { getArticleByID, match: { params } } = this.props;
        getArticleByID(params.id);
    }

    onEditClick() {
        const { currentArticle: { id } } = this.props;
        redirectAPI(`/posts/${id}/edit`);
    }

    onDeleteClick() {
        const { currentArticle: { id }, deleteArticle } = this.props;
        deleteArticle(id);
    }

    render() {
        const { currentArticle, isAdmin } = this.props;

        if (!currentArticle) {
            return null;
        }

        const { body, bodyType, publishedDate, title } = currentArticle;

        var bodyHTML;
        if (bodyType === MARKDOWN_BODY_TYPE) {
            bodyHTML = MARKDOWN_CONVERTER.makeHtml(body);
        } else {
            bodyHTML = body;
        }

        const formatedDate = format(publishedDate, 'D/MM/YYYY');
        if (!formatedDate || formatedDate == 'Invalid Date') {
            return null;
        }

        return (
            <div className="container-fluid">
                <div className="col">
                    <h4>{title}</h4>
                    <div><small>{formatedDate}</small></div>
                    <hr />
                    <div dangerouslySetInnerHTML={{ __html: bodyHTML }} />
                </div>
                {isAdmin ? (
                    <div className="media row">
                        <div className="col-4"></div>
                        <div className="col-1">
                            <button className="btn btn-dark btn-lg" onClick={this.onEditClick}>EDIT</button>
                        </div>
                        <div className="col-1">
                            <button className="btn btn-danger btn-lg" onClick={this.onDeleteClick}>DELETE</button>
                        </div>
                    </div>
                ) : null}
            </div>
        );
    }
}

ConnectedArticleDetail.propTypes = {
    isAdmin: PropTypes.bool.isRequired,
    currentArticle: PropTypes.object
}

const ArticleDetail = connect(mapStateToProps, mapActionCreators)(ConnectedArticleDetail);
export default ArticleDetail;