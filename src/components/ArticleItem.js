import React from 'react';
// import { Link } from 'react-router-dom'
import { format } from 'date-fns';

const ArticleItem = ({ id, title, publishedDate }) => (
    <h5>
        <a href={`${getAPIURL()}/posts/${id}`} className="text-dark">{title}</a>&nbsp;
        {/* <Link to={`/articles/${id}`} className="text-dark">{title}</Link>&nbsp; */}
        <small className="font-italic text-muted">{format(publishedDate, 'D/MM/YYYY')}</small>
    </h5>
);

export default ArticleItem;