import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getArticles } from '../reducers/articles';
import ArticleItem from './ArticleItem';

import './Articles.css';

const mapStateToProps = state => ({
  articles: state.articles.articles
});

const mapActionCreators = {
  getArticles
};

class ConnectedArticles extends Component {
  componentDidMount() {
    this.props.getArticles();
  }

  render() {
    const { articles } = this.props;
    return (
      <div className="container-fluid">
        <h2 className="Page-title">Stream of thoughts</h2>
        <div>
          <ul className="list-group list-group-flush">
            {articles.map(el => (
              <li className="list-group-item" key={el.id}>
                <ArticleItem
                  id={el.id}
                  title={el.title}
                  publishedDate={el.publishedDate}
                />
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

ConnectedArticles.propTypes = {
  articles: PropTypes.array.isRequired
}

const Articles = connect(mapStateToProps, mapActionCreators)(ConnectedArticles);
export default Articles;