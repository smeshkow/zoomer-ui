import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { importCSV } from '../reducers/shimper';
import { getAPIURL } from '../config/index';
import axios, { post } from 'axios';
import ShimperStatus from './ShimperStatus';

const mapStateToProps = state => ({
    importStatus: state.shimper.importStatus
});

const mapActionCreators = {
    importCSV
};

class ConnectedShimper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: null,
            error: null
        }
        this.onFormSubmit = this.onFormSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
        this.fileUpload = this.fileUpload.bind(this)
    }

    onFormSubmit(e) {
        e.preventDefault(); // Stop form submit
        this.fileUpload(this.state.file).then((response) => {
            this.setState(prevState => ({
                ...prevState,
                error: response.data
            }))
        })
    }

    onChange(e) {
        this.setState({ file: e.target.files[0] });
    }

    fileUpload(file) {
        const url = getAPIURL() + '/api/shimper/import/csv';
        const formData = new FormData();
        formData.append('file', file);
        const config = {
            headers: {
                'content-type': 'multipart/form-data'
            },
            withCredentials: true
        }
        return post(url, formData, config);
    }

    render() {
        const { user } = this.props;
        const isAuthenticated = !!(user && user.id);

        if (!isAuthenticated) {
            return null;
        }

        return (
            <div className="container">
                <form onSubmit={this.onFormSubmit}>
                    <div className="form-group">
                        <label htmlFor="title">Import file</label>
                        <input type="file" onChange={this.onChange} className="form-control-file" />
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-success btn-lg">Upload</button>
                    </div>
                    {this.state.error ? (
                        <div className="alert alert-danger" role="alert">
                            [failed] <span>{this.state.error}</span>
                        </div>
                    ) : null}
                    <ShimperStatus />
                </form>
            </div>
        )
    }
}

ConnectedShimper.propTypes = {
    user: PropTypes.object,
}

const Shimper = connect(mapStateToProps, mapActionCreators)(ConnectedShimper);
export default Shimper;