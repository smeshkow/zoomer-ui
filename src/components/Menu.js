import React from 'react';
import { Link } from 'react-router-dom'
import { getAPIURL } from '../config/index';

const Menu = ({ user }) => (
    <ul className="navbar-nav mr-auto">
        <li className="nav-item">
            <a href={`${getAPIURL()}/blog`} className="nav-link">Blog</a>
            {/* <Link to="/articles" className="nav-link">Blog</Link> */}
        </li>
        <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">Tagify</a>
            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <Link className="dropdown-item" to="/tagify/about">About</Link>
                <Link className="dropdown-item" to="/tagify">Playground</Link>
                {user && user.id ? (<div className="dropdown-divider"></div>) : null}
                {user && user.id ? (<Link className="dropdown-item" to="/tagify/register">Register site</Link>) : null}
            </div>
        </li>
        {user && user.isAdmin ?
            (
                <li className="nav-item">
                    <a href={`${getAPIURL()}/blog/add`} className="nav-link">Add article</a>
                </li>
            )
            : null}
    </ul>
);

export default Menu;