import React from 'react';

import './Main.css';

const Main = () => (
    <div className="container-fluid">
        <div className="row align-items-center">
            <div className="col">
                <div className="media justify-content-center">
                    <p>Welcome to <b>ZoomIO!</b></p>
                </div>
                <div className="media justify-content-center">
                    <p>Curious about what does it mean?</p>
                </div>
                <div className="media justify-content-center">
                    <p>This is just the page for you - 
                        <a href="https://zoomio.org/blog/post/what_is_zoomio-5630455256711168" className="text-dark">
                            "What is ZoomIO?"
                        </a>
                    </p>
                </div>
                <div className="media justify-content-center">
                    <img
                        src="https://storage.googleapis.com/www.zoomio.org/img/ZoomIO.JPG"
                        className="img-responsive center-block zoomio-img" />
                </div>
            </div>
        </div>
    </div>
);

export default Main;