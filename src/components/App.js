import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AppHeader from './AppHeader'
import AppFooter from './AppFooter';
import Articles from './Articles';
import ArticleDetail from './ArticleDetail';
import ArticleEditForm from './ArticleEditForm';
import ArticleForm from './ArticleForm';
import Main from './Main';
import Shimper from './Shimper';
// import TagifyArticleList from './TagifyArticleList';
import Tagify from './Tagify';
import TagifyAbout from './TagifyAbout';
import TagifySiteRegister from './TagifySiteRegister';
import { getUser } from '../reducers/user';

const mapStateToProps = state => ({
  user: state.user.user
});

const mapActionCreators = {
  getUser
};

class ConnectedApp extends Component {
  componentDidMount() {
    this.props.getUser();
  }

  render() {
    const { user } = this.props;
    return (
      <Router>
        <div>
          <AppHeader user={user} />
          <Switch>
            <Route path="/" exact component={Main} />
            <Route path="/shimper" exact render={props => (
              <Shimper {...props} user={user} />
            )} />
            <Route path="/articles" exact component={Articles} />
            <Route path="/articles/add" exact render={props => (
              <ArticleForm {...props} user={user} />
            )} />
            <Route path="/articles/:id/edit" render={props => (
              <ArticleEditForm {...props} user={user} />
            )} />
            <Route path="/articles/:id" render={props => (
              <ArticleDetail {...props} isAdmin={!!(user && user.isAdmin)} />
            )} />
            <Route path="/tagify" exact render={props => (
              <Tagify {...props} />
            )} />
            <Route path="/tagify/about" exact render={props => (
              <TagifyAbout {...props} />
            )} />
            <Route path="/tagify/register" exact render={props => (
              <TagifySiteRegister {...props} isAuthenticated={!!(user && user.id)} />
            )} />
            {/* <Route path="/tagify/:value" render={props => (
              <TagifySiteRegister {...props} isAuthenticated={!!(user && user.id)} />
            )} /> */}
            <Redirect to="/" push={false} />
          </Switch>
          <AppFooter />
        </div>
      </Router>
    );
  }
}

ConnectedApp.propTypes = {
  getUser: PropTypes.func.isRequired,
  user: PropTypes.object,
}

const App = connect(mapStateToProps, mapActionCreators)(ConnectedApp);

export default App;