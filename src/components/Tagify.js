import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchTags } from '../reducers/tagify';
import { getAPIURL, getTagifyPlaygroundID } from '../config/index';

import './Tagify.css';

const apiURL = getAPIURL();
const playgroundID = getTagifyPlaygroundID();

const mapStateToProps = state => ({
    tags: state.tagify.tags,
    pageTitle: state.tagify.title,
    pageSource: state.tagify.source,
    isLoading: state.tagify.isLoading
});

const mapActionCreators = {
    fetchTags
};

class ConnectedTagify extends Component {
    constructor(props) {
        super(props);
        this.state = {
            source: 'https://google.com',
            limit: 10,
            query: '',
            loading: props.isLoading,
            tagMap: new Map()
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleTagClick = this.handleTagClick.bind(this);
    }

    componentDidUpdate() {
        const { isLoading, tags, pageTitle } = this.props;
        const { loading, limit, source, query, tagMap } = this.state;

        if (isLoading !== loading) {
            if (tags && tags.length > 0) {
                tagMap.set(source, { limit, source, query, tags, pageTitle });
            }
            this.setState({
                loading: isLoading,
                tagMap: tagMap
            });
        }
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        const { limit, source, query } = this.state;
        this.props.fetchTags({ source, limit, query });
        this.setState({
            loading: true
        });
    }

    handleTagClick(event) {
        event.preventDefault();
        const tagValue = event.target.getAttribute('data');

        const { tagMap } = this.state;
        if (tagMap && tagMap.size() == 0) {
            return;
        }

        this.setState({
            loading: true
        });

        tagMap.forEach((k, v, m) => {
            if (!v.tags.find(tag => tagValue === tag.value)) {
                m.delete(k);
            }
        });

        this.setState({
            loading: false,
            tagMap: tagMap
        });
    }

    render() {
        const { source, limit, query, loading, tagMap } = this.state;

        const tagList = tagMap && Array.from(tagMap.values());

        return (
            <div className="container">
                <h1>Tagify playground</h1>
                <div className="alert alert-primary" role="alert">
                    You can read more about "Tagify" on the <a href="/tagify/about">"About"</a> page.<br />
                </div>
                <div className="alert alert-info" role="info">
                    Paste a link into the "Source" and click "Tagify" to see the top tags from the source.
                </div>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="source">Source (URL to query from)</label>
                        <input
                            type="text"
                            className="form-control"
                            id="source"
                            name="source"
                            value={source}
                            onChange={this.handleChange}
                        />
                        <label htmlFor="source">Optional - Limit (specify amount of tags)</label>
                        <input
                            type="text"
                            className="form-control"
                            id="limit"
                            name="limit"
                            value={limit}
                            onChange={this.handleChange}
                        />
                        <label htmlFor="query">Optional - CSS selector (target specific HTML tag)</label>
                        <input
                            type="text"
                            className="form-control"
                            id="query"
                            name="query"
                            value={query}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-success btn-sm">Tagify</button>
                    </div>
                </form>
                {loading ? (
                    <div className="spinner-border m-5" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                ) : tagList && tagList.length > 0 ? (
                    <div>
                        Result:
                        {tagList.map((el, i) => (
                            <div key={i}>
                                <hr />
                                <div>
                                    <h5>
                                        <a target="_blank" href={el.source}>
                                            {el.pageTitle ? `"${el.pageTitle}" - ${el.source}` : el.source}
                                        </a>
                                    </h5>
                                </div>
                                <div><p>(fetched {el.limit} tags)</p></div>
                                <ul className="nav">
                                    {el.tags.map((elm, i) => (
                                        <li className="nav-item" key={i}>
                                            <a
                                                className="nav-link text-secondary"
                                                href={`${apiURL}/api/tagify/value/${playgroundID}?value=${elm.value}&limit=10&redirect=${apiURL}/tagify/relevant`}>
                                                #{elm.value}
                                            </a>
                                            {/* <a
                                                className="nav-link text-secondary"
                                                href="/"
                                                data={elm.value}
                                                onClick={this.handleTagClick}>#{elm.value}</a> */}
                                        </li>
                                    ))}
                                </ul>
                                <hr />
                            </div>
                        ))}
                    </div>
                ) : null}
                <div className="after-how-to">&nbsp;</div>
            </div>
        );
    }
}

ConnectedTagify.propTypes = {
    tags: PropTypes.array,
    pageTitle: PropTypes.string,
    pageSource: PropTypes.string,
    isLoading: PropTypes.bool,
}

const Tagify = connect(mapStateToProps, mapActionCreators)(ConnectedTagify);
export default Tagify;