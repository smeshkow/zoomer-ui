import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Redirect } from 'react-router';
import { Route } from 'react-router-dom';

class PrivateRoute extends Component {
  // componentDidMount() {
  //   const { isAuthenticated } = this.props;
  //   if (!isAuthenticated) {
  //     login();
  //   }
  // }

  render() {
    const { isAuthenticated, component, ...rest } = this.props;
    const PrivateComponent = component;

    // if (!isAuthenticated) {
    //   return (
    //     <Redirect to="/" push={false} />
    //   );
    // }

    return (
      <Route {...rest} render={props => (
        <PrivateComponent {...props} />
      )} />
    );
  }
}

PrivateRoute.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  path: PropTypes.string.isRequired,
  exact: PropTypes.bool.isRequired,
  component: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.instanceOf(Component)
  ]).isRequired,
}

export default PrivateRoute;