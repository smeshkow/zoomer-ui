import RestClient from './RestClient';
import { getTagifyURL, getTagifyAppID, getTagifyPlaygroundID } from '../config/index';

export interface TokenResponse {
    token: string;
    expiry: string;
}

export interface TagifyRegisterResponse {
    host: string;
    id: string;
}

export interface TagItem {
    id?: string;
    value?: string;
    source?: string;
    pageTitle?: string;
    score?: number;
    count?: number;
    timestamp?: string;
}

export interface TagItemsResponse {
    data: {
        tags: TagItem[]
    }
}

export interface TagifyResponseItem {
    source: string;
    title: string;
    tags: TagItem[];
}

export interface TagsBatchResponse {
    data: {
        pages: TagifyResponseItem[];
    }
}

export interface TagifyRequestItem {
    source: string;
    title: string;
    limit: number;
}

class TagifyClient extends RestClient {

    private playgroundID: string;

    constructor({ playgroundID = '', serviceUrl = '', onUnauthorised = () => { } } = {}) {
        super({ serviceUrl, onUnauthorised });
        this.playgroundID = playgroundID;
    }

    getToken(): Promise<TokenResponse> {
        return this.getResource('/api/token');
    }

    registerSite(host: string): Promise<TagifyRegisterResponse> {
        return this.postResource('/api/tagify/register', { host });
    }

    fetchTags(request: { source: string, limit: number, query: string }): Promise<TagsBatchResponse> {
        const { source, limit, query } = request;
        const path = `/api/tagify?appID=${this.playgroundID}&source=${source}&limit=${limit}&query=${query}`;
        return this.getResource(path, {
            credentials: 'omit',
            headers: {
                Accept: 'application/json',
            },
            mode: 'cors',
        });
    }
}

/**
 * REST Client to make calls to resources on the Token service.
 *
 * @type {RestClient}
 */
export default new TagifyClient({
    playgroundID: getTagifyPlaygroundID(),
    serviceUrl: getTagifyURL(),
});