import RestClient from './RestClient';
import { getAPIURL } from '../config/index';

class ArticlesClient extends RestClient {
  getArticles() {
    return this.getResource('/api/articles');
  }

  getArticleByID(articleID) {
    return this.getResource(`/api/articles/${articleID}`);
  }

  addArticle(article) {
    return this.postResource('/api/articles', article);
  }

  deleteArticle(articleID) {
    return this.deleteResource(`/api/articles/${articleID}`);
  }
}

/**
 * REST Client to make calls to resources on the Zoomee.io service.
 *
 * @type {RestClient}
 */
export default new ArticlesClient({
  serviceUrl: getAPIURL(),
});
