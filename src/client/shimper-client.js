import RestClient from './RestClient';
import { getAPIURL } from '../config/index';

class ShimperClient extends RestClient {
  importCSV(csv) {
    return this.postResource('/api/shimper/import/csv', csv);
  }

  getImportStatus() {
    return this.getResource('/api/shimper/import/status')
  }
}

/**
 * REST Client to make calls to resources on the Zoomee.io service.
 *
 * @type {RestClient}
 */
export default new ShimperClient({
  serviceUrl: getAPIURL(),
});
