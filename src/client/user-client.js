import RestClient from './RestClient';
import { getAPIURL } from '../config/index';

class UserClient extends RestClient {
  getUser() {
    return this.getResource('/api/user');
  }
}

/**
 * REST Client to make calls to resources on the Zoomee.io service.
 *
 * @type {RestClient}
 */
export default new UserClient({
  serviceUrl: getAPIURL(),
});
