import { getAPIURL, getUIURL } from '../config/index';

/**
 * Helpers and abstractions.
 */

export function isEmptyObject(obj) {
    Object.keys(obj).length === 0 && obj.constructor === Object
}

export function redirect(hostname) {
    window.location.assign(hostname);
}

export function redirectAPI(path) {
    window.location.assign(`${getAPIURL()}${path}`);
}

export function redirectUI(path) {
    window.location.assign(`${getUIURL()}${path}`);
}

export function locationHref() {
    return window.location.href;
}

export function schema() {
    return location.protocol;
}

export function login() {
    redirect(`${getAPIURL()}/login?redirect=${encodeURIComponent(locationHref())}`);
}

export function logout() {
    redirect(`${getAPIURL()}/logout?redirect=${encodeURIComponent(getUIURL())}`);
}
