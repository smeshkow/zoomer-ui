type Env = 'development' | 'production';

const getEnv = (): Env => {
    return __DEV__ ? 'development' : 'production';
}

const config = {
    development: {
        api: 'http://localhost:8080',
        web: 'http://app.mindiai.ai',
        tagifyAppID: 'd558e00b-31e6-4699-911a-68a131312f7f',
        tagifyPlaygroundID: 'd558e00b-31e6-4699-911a-68a131312f7f',
    },
    production: {
        api: 'https://zoomio.org',
        web: 'https://www.zoomio.org',
        tagifyAppID: 'ff3ee693-acdf-4216-88b7-7b3e4dbe3907',
        tagifyPlaygroundID: '07553a4b-32cb-466d-9ec6-bebd43c2bcd8',
    }
};

export const getAPIURL = (): string => {
    return config[getEnv()].api;
}

export const getUIURL = (): string => {
    return config[getEnv()].web;
}

export const getTagifyURL = (): string => {
    return config[getEnv()].api;
}

export const getTagifyAppID = (): string => {
    return config[getEnv()].tagifyAppID;
}

export const getTagifyPlaygroundID = (): string => {
    return config[getEnv()].tagifyPlaygroundID;
}