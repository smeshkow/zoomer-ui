import articlesClient from "../client/articles-client"

// Constants
const ADD_ARTICLE = "ADD_ARTICLE";
const GET_ARTICLE = "GET_ARTICLE";
const GET_ARTICLE_LIST = "GET_ARTICLE_LIST";
const DELETE_ARTICLE = "DELETE_ARTICLE";

// Actions
export function addArticle(article) {
    return async (dispatch, getState) => {
        const { isLoading } = getState().articles;

        // Only have one request in-flight at a time.
        if (isLoading) {
            return;
        }

        const action = {
            type: ADD_ARTICLE,
            payload: articlesClient.addArticle(article)
        };
        dispatch(action);
    };
}

export function getArticles() {
    return async (dispatch, getState) => {
        const { isLoading } = getState().articles;

        // Only have one request in-flight at a time.
        if (isLoading) {
            return;
        }

        const articles = await articlesClient.getArticles();

        dispatch({
            type: GET_ARTICLE_LIST,
            payload: {
                articles
            }
        });
    };
}

export function getArticleByID(articleID) {
    return async dispatch => {
        return dispatch({
            type: GET_ARTICLE,
            payload: articlesClient.getArticleByID(articleID)
        });
    };
}

export function deleteArticle(articleID) {
    return async (dispatch, getState) => {
        const { isLoading } = getState().articles;

        // Only have one request in-flight at a time.
        if (isLoading) {
            return;
        }

        await articlesClient.deleteArticle(articleID);

        const action = {
            type: DELETE_ARTICLE,
            payload: articleID
        };
        dispatch(action);
    };
}

// Reducer
const initialState = {
    articles: [],
    currentArticle: {
        id: '',
        title: '',
        publishedDate: '',
        imageURL: '',
        body: '',
        bodyType: '',
        bodyHTML: ''
    },
    isError: false,
    isLoading: false
};

const articlesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_ARTICLE:
            return { ...state, articles: [...state.articles, action.payload] };
        case `${GET_ARTICLE_LIST}`:
            const { articles } = action.payload;
            return {
                ...state,
                articles,
                isError: false,
                isLoading: false
            }
        case `${GET_ARTICLE}_PENDING`:
            return {
                ...state,
                isError: false,
                isLoading: true,
            };
        case `${GET_ARTICLE}_REJECTED`:
            return {
                ...state,
                isError: true,
                isLoading: false
            };
        case `${GET_ARTICLE}_FULFILLED`:
            return {
                ...state,
                currentArticle: action.payload,
                isError: false,
                isLoading: false
            }
        case `${DELETE_ARTICLE}_PENDING`:
            return {
                ...state,
                isError: false,
                isLoading: true,
            };
        case `${DELETE_ARTICLE}_REJECTED`:
            return {
                ...state,
                isError: true,
                isLoading: false
            };
        case `${DELETE_ARTICLE}_FULFILLED`:
            return {
                ...state,
                articles: state.articles.filter(article => article.id !== action.payload),
                isError: false,
                isLoading: false
            }
        default:
            return state;
    }
};

export default articlesReducer;
