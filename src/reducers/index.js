import { combineReducers } from 'redux';
import articlesReducer from './articles';
import shimperReducer from './shimper';
import tagifyReducer from './tagify';
import userReducer from './user';

export function makeRootReducer() {
  return combineReducers({
    user: userReducer,
    tagify: tagifyReducer,
    articles: articlesReducer,
    shimper: shimperReducer,
  });
};
