import tagifyClient from '../client/TagifyClient';

// ------------------------------------
// Constants
// ------------------------------------
export const GET_TOKEN = 'GET_TOKEN';
export const REGISTER_SITE = 'REGISTER_SITE';
export const FETCH_TAGS = 'FETCH_TAGS';
export const GET_TAGS_BATCH = 'GET_TAGS_BATCH';

export const TAGS_LIMIT = 4;

// ------------------------------------
// Actions
// ------------------------------------
export function getToken() {
  return async dispatch => {
    const action = {
      type: GET_TOKEN,
      payload: tagifyClient.getToken()
    };
    dispatch(action);
  };
}

export function registerSite(site, token) {
  return async dispatch => {
    const action = {
      type: REGISTER_SITE,
      payload: tagifyClient.registerSite(site, token)
    };
    dispatch(action);
  };
}

export function fetchTags(props) {
  return async dispatch => {
    const action = {
      type: FETCH_TAGS,
      payload: tagifyClient.fetchTags({ limit: TAGS_LIMIT, ...props })
    };
    dispatch(action);
  };
}

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
  token: {},
  registered: {},
  tags: [],
  title: '',
  source: '',
  pages: [],
  hasLoaded: false,
  isLoading: false,
  isError: false,
};

// Map REST payload to domain model
const tokenDataFromPayload = payload => ({
  token: payload.token,
  expiry: new Date(payload.expiry),
});

const dataToTags = data => {
  const { pages = [] } = data;
  if (!pages || !pages[0].tags) {
    return [];
  }
  return pages[0];
}

export default function tagifyReducer(state = initialState, action) {
  switch (action.type) {
    case `${GET_TOKEN}_PENDING`:
      return {
        ...state,
        isLoading: true,
      };
    case `${GET_TOKEN}_FULFILLED`:
      return {
        ...state,
        token: tokenDataFromPayload(action.payload),
        isLoading: false,
        isError: false,
      };
    case `${GET_TOKEN}_REJECTED`:
      return {
        ...state,
        isLoading: false,
        isError: true,
      };
    case `${REGISTER_SITE}_PENDING`:
      return {
        ...state,
        isLoading: true,
      };
    case `${REGISTER_SITE}_FULFILLED`:
      return {
        ...state,
        registered: action.payload,
        isLoading: false,
        isError: false,
      };
    case `${REGISTER_SITE}_REJECTED`:
      return {
        ...state,
        registered: null,
        isLoading: false,
        isError: true,
      };
      case `${FETCH_TAGS}_PENDING`:
        return {
          ...state,
          isLoading: true,
        };
      case `${FETCH_TAGS}_FULFILLED`:
        const { tags, title, source } = dataToTags(action.payload.data)
        return {
          ...state,
          tags,
          title,
          source,
          isLoading: false,
          isError: false,
          hasLoaded: true,
        };
      case `${FETCH_TAGS}_REJECTED`:
        return {
          ...state,
          isLoading: false,
          isError: true,
          hasLoaded: true,
        };
    default:
      return state;
  }
}