import shimperClient from "../client/shimper-client"

// Constants
const IMPORT_CSV = "IMPORT_CSV";
const IMPORT_STATUS = "IMPORT_STATUS";

// Actions
export function importCSV(csv) {
    return async (dispatch, getState) => {
        const action = {
            type: IMPORT_CSV,
            payload: shimperClient.importCSV(csv)
        };
        dispatch(action);
    };
}

export function getImportStatus() {
    return async (dispatch, getState) => {
        const action = {
            type: IMPORT_STATUS,
            payload: shimperClient.getImportStatus()
        };
        dispatch(action);
    };
}

// Reducer
const initialState = {
    importStatus: {
        isInProgres: false,
        error: null,
        input: {
            fileName: '',
            records: 0
        },
        result: {
            updates: 0,
            failed: []
        }
    },
    isError: false,
    isLoading: false
};

const shimperReducer = (state = initialState, action) => {
    switch (action.type) {
        case `${IMPORT_CSV}_PENDING`:
            return {
                ...state,
                isError: false,
                isLoading: true,
            };
        case `${IMPORT_CSV}_REJECTED`:
            return {
                ...state,
                isError: true,
                isLoading: false
            };
        case `${IMPORT_CSV}_FULFILLED`:
            return {
                ...state,
                isError: false,
                isLoading: false
            }
        case `${IMPORT_STATUS}_PENDING`:
            return {
                ...state,
                isError: false,
                isLoading: true,
            };
        case `${IMPORT_STATUS}_REJECTED`:
            return {
                ...state,
                isError: true,
                isLoading: false
            };
        case `${IMPORT_STATUS}_FULFILLED`:
            return {
                ...state,
                importStatus: action.payload,
                isError: false,
                isLoading: false
            }
        default:
            return state;
    }
};

export default shimperReducer;