import userClient from '../client/user-client';

// Constants
const GET_USER = 'GET_USER';

// Actions
export function getUser() {
    return async dispatch => {
        const action = {
            type: GET_USER,
            payload: userClient.getUser(),
        };
        dispatch(action);
    };
}

// Reducer
const initialState = {
    user: null,
    isError: false,
    isLoading: false
};

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case `${GET_USER}_PENDING`:
            return {
                ...state,
                isError: false,
                isLoading: true,
            };
        case `${GET_USER}_REJECTED`:
            return {
                ...state,
                user: null,
                isError: true,
                isLoading: false
            };
        case `${GET_USER}_FULFILLED`:
            return {
                ...state,
                user: action.payload,
                isError: false,
                isLoading: false
            };
        default:
            return state;
    }
};

export default userReducer;