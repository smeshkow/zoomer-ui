# ZoomIO UI

A very messy FE for [ZoomIO](https://www.zoomio.org/), a personal web-site/playground for my pet projects. If you are here for clean/idomatic JS/React, you are definitely in the wrong place. It is minimal effort - maximum value kind of project, a quick MVP as you like. So again don't judge.

## Requirements

* React
* Redux
* [Webpack](https://webpack.js.org/guides/installation/#local-installation)
* [Babel](https://babeljs.io/docs/setup/#installation)

To kick things off you might want to check out this [Cool tutotrial](https://www.valentinog.com/blog/react-redux-tutorial-beginners/#React_Redux_tutorial_what_you_will_learn).